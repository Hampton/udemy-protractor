//using jasmin as test framework
//describe and it come from jasmin
var helper = require('./helper');

describe('Main page : login/register', function(){
//   it('should have a title', function(){
//     browser.get('http://juliemr.github.io/protractor-demo/');
//
//     expect(browser.getTitle()).toEqual('Super Calculator');
//   });
// })
// it('should have a title', function(){
//   browser.get(browser.params.url);
//
//   var title = 'Etsy :: Your place to buy and sell all things handmade';
//   expect(browser.getTitle()).toEqual(title);
//   });

it('should sign in and verify that password error appear', function() {
  browser.get(browser.params.url);
//can use by.css, by.xpath, by.linkedText etc..
  var signInButton = element(by.id('sign-in'));
  var usernameField = element(by.id('username-existing'));
  var userPasswordField = element(by.id('password-existing'));
  var signInLoginForm = element(by.id('signin-button'));
  var passwordExistingError = element(by.id('password-existing-error'));

  signInButton.click();
  helper.waitUntilReady(usernameField);
  //usernameField.clear();
  usernameField.sendKeys('test@test.com');
  helper.waitUntilReady(userPasswordField);
  userPasswordField.sendKeys('password');
  signInLoginForm.click();

  helper.waitUntilReady(passwordExistingError);

  //var passwordExistingErrorText = passwordExistingError.getText();

  expect(passwordExistingError.getText()).toBe('Password was incorrect.');
});

it('should register a new user', function() {
  browser.get(browser.params.url);

  var registerButtonOnMainPage = element(by.id('register'));
  var firstNameField = element(by.id('first-name'));
  var lastNameField = element(by.id('last-name'));
  var emailField = element(by.id('email'));
  var passwordField = element(by.id('password'));
  var passwordConfirmField = element(by.id('password-repeat'));
  var usernameRegisterField = element(by.id('username'));
  var etsy_finds = element(by.id('etsy_finds'));
  var registerButtonRegisterPopup = element(by.id('register_button'));

  registerButtonOnMainPage.click()
  helper.waitUntilReady(firstNameField)
  firstNameField.sendKeys('QA')
  lastNameField.sendKeys('Automation')
  emailField.sendKeys('test@test.com')
  passwordField.sendKeys('password')
  passwordConfirmField.sendKeys('password')
  usernameRegisterField.sendKeys('qa_test')
  etsy_finds.click()
  helper.waitUntilReady(registerButtonRegisterPopup)

  //registerButtonOnMainPage.click()

});

});
