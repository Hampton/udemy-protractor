exports.config = {
  framework: 'jasmine',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['spec.js'],
  //params is an object
    params: {
      url: 'https://www.etsy.com/'
    },
    onPrepare: function() {
      browser.ignoreSynchronization = true;
    }
  }
//./node_modules/protractor/bin/protractor conf.js
//webdriver-manager start
